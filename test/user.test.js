process.env.NODE_ENV = "test"

const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const { GET_USERS, DELETE_USER } = require('../const/index');

const should = chai.should();

chai.use(chaiHttp)

describe('/GET users', () => {
    it('Get all users', (done) => {
        chai.request(app)
        .get(GET_USERS)
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            done();
        });
    });
});

describe('/DELETE users', () => {
    it('Delete user by id', (done) => {
        chai.request(app)
        //.del(DELETE_USER).send('1')
        .del('/users/1')
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            done();
        });
    });
});