process.env.NODE_ENV = "test";

const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../app");
const { GET_NEWS, GET_ENTRY } = require("../const/index");

const should = chai.should();

chai.use(chaiHttp);

describe("/GET news", () => {
  it("Get all news", (done) => {
    chai
      .request(app)
      .get(GET_NEWS)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        done();
      });
  });
});

describe("/GET entries", () => {
  it("Get entry by id", (done) => {
    chai
      .request(app)
      .get(GET_ENTRY)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        done();
      });
  });
});

describe('/DELETE news', () => {
  it('Delete news by id', (done) => {
      chai.request(app)
      //.del(DELETE_NEWS).send('1')
      .del('/news/1')
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
      });
  });
});
