const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../app");
const { 
  TEST_GET_ACTIVITIES,
  TEST_GET_ACTIVITY, 
  TEST_POST_ACTIVITY,
  TEST_UPDATE_ACTIVITY
} = require("../const/index");
const { SUCCESS_CODE } = require('../const/const');
const should = chai.should();

chai.use(chaiHttp);

describe('/GET activities', () => {
  it('Get activities', (done) => {
    chai
      .request(app)
      .get(TEST_GET_ACTIVITIES)
      .end((err, res) => {
          res.should.have.status(SUCCESS_CODE);
          res.body.should.be.a('object');
          done();
      });
  });
});

describe('/GET activity', () => {
  it('Get activity by id', (done) => {
    chai
      .request(app)
      .get(TEST_GET_ACTIVITY)
      .end((err, res) => {
          res.should.have.status(SUCCESS_CODE);
          res.body.should.be.a('object');
          done();
      });
  });
});

describe("/POST activity", () => {
  it("Create Activity", (done) => {
    chai
      .request(app)
      .post(TEST_POST_ACTIVITY)
      .send({
        name: 'Activity',
        image: 'imageActivity.png',
        content: 'content activity'
      })
      .end((err, res) => {
        res.should.have.status(SUCCESS_CODE);
        res.body.should.be.a("object");
        done();
      });
  });
});

describe("/UPDATE activity", () => {
  it("UPDATE Activity", (done) => {
    chai
      .request(app)
      .put(TEST_UPDATE_ACTIVITY)      
      .send({
        name: 'Activity',
        image: 'imageActivity.png',
        content: 'content activity'
      })
      .end((err, res) => {
        res.should.have.status(SUCCESS_CODE);
        res.body.should.be.a("object");
        done();
      });
  });
});