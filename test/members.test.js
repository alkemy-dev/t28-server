const chai = require("chai");
const chaiHttp = require("chai-http");
const http = require('http');
const app = require('../app');

chai.use(chaiHttp);

const expect = require('chai').expect;

var test_id = 0;
var test_length = 0;

// Test Members endpoint's Suite with moka and chai




describe('Test of Member\'s End Points', () => { // <-- Describe testing group
	// create a server to close connections correctly and avoid mocha's hang ups
	const server = http.createServer(app);
	
	before( (done) => setTimeout(done, 1500) ); // <- delay test start
	
	after(done => server.close(done)); // <- close connections after tests has finished
	

// GET ALL MEMBERS
	it('Should get all members', (done) => { // <-- individual test
		chai.request(server)
			.get('/members')
			.end( function(err, res) {
				console.log('GEt Members Body ',res.body);
				expect(res, 'Get members list from the server').to.have.status(200);
				test_length = res.body.data.length;
				done();
			})
	})

// CREATE MEMBER
//membersRoutes.post("/", members_validation, members_post);

	it('Create new member', (done) => {
		chai.request(server)
			.post('/members')
			.send({name: 'Test-Member', image: 'members/test-image.url'})
			.end( (err, res) => {
				console.log('POST new member body: ',res.body);
				expect(res).to.have.status(200);
				test_id = res.body.data.id; // <-- for working with real data
				done();
			})
	})

//GET member by id

	it('Get member by id', (done) => {
		chai.request(server)
			.get(`/members/${test_id}`)
			.end( (err, res) => {
				console.log('Single Member res.body ',res.body);
				expect(res.body.data).to.have.property('id').to.be.equal(test_id);
				expect(res).to.have.status(200);
				done();
			})
	})

// UPDATE MEMBER
//membersRoutes.put("/:id", members_validation, members_update, members_find_by_id);

	it('Update member by id', (done) => {
		chai.request(server)
			.put(`/members/${test_id}`)
			.send({name: 'Updated-Member'})
			.end( (err, res) => {
				console.log('Updated member.body', res.body);
				expect(res.body.data).to.have.property('name').to.be.equal('Updated-Member');
				expect(res).to.have.status(200);
				done();
			})
	})

// DELETE MEMBER BY ID
//membersRoutes.delete("/:id", members_delete);

	it('Destroy single member', (done) => {
		chai.request(server)
			.del(`/members/${test_id}`)
			.end( (err, res) => {
				console.log('DELETE res.body: ',res.body);
				expect(res).to.have.status(200);
				chai.request(app)
					.get('/members')
					.end( (err, res) => {
						expect(res.body.data).to.have.lengthOf(test_length);
						expect(res).to.have.status(200);
						done();
					})
			})
	})
	

// CREATE new Member with missing attribute type
	it('Creates New Member with missing Attribute', (done) => {
		chai.request(server)
			.post('/members')
			.send({image: 'img.magic.url'})
			.end( (err, res) => {
				console.log('POST Member with WRONG VALUE ERROR ', err, '\nRESPONSE ', res.body);
				expect(res).not.to.have.status(200);
				done();
			})
	})

// GET member with wrong ID
	it('GET Member with wrong ID ERROR ', (done) => {
		chai.request(server)
			.get('members/tyr')
			.end( (err, res) => {
				console.log('GET Member with WRONG ID ERROR ',err, '\nRESPONSE ', res);
				expect(err).not.to.be.null;
				done();
			})
	})

// PUT Member with unexisting ID
	it('PUT Member with wrong ID', (done) => {
		chai.request(server)
			.put('members/t77t')
			.send({name: 'WrongID Member', image:'wrongImg.URL'})
			.end( (err, res) => {
				console.log('PUT WITH WRONG ID ERROR ', err, '\nRESPONSE ', res);
				expect(err).not.to.be.null;
				done();
			})
	});

// DELETE Unexisting Member's ID
	it('DELETE Member with wrong ID', (done) => {
		chai.request(server)
			.del('members/3r')
			.end( (err, res) => {
				console.log('DELETE WITH WRONG ID ERROR ', err, '\nRESPONSE ', res);
				expect(err).not.to.be.null;
				done()
			})
	});

});



