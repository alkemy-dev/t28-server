const request = require("supertest");
const app = require("../app");
const db = require("../models");
const validUser = {
  email: "a@a1.com",
  password: "1234567",
};

describe("Login user Auth", () => {
  test("Valid user login", async () => {
    const res = await request(app).post("/auth/login").send(validUser);
    await expect(res.status).toBe(200);
  });
  test("Valid user TOKEN", async () => {
    const res = await request(app).post("/auth/login").send(validUser);
    await expect(typeof res.body.token).toBe("string");
  });
  test("Invalid user EMAIL", async () => {
    let invalidUser = { ...validUser, email: "@a1.com" };

    console.log(invalidUser);
    const res = await request(app).post("/auth/login").send(invalidUser);
    await expect(res.body.status).toBe(400);
  });
  test("Invalid user Password", async () => {
    let invalidUser = { ...validUser, password: "123" };
    const res = await request(app).post("/auth/login").send(invalidUser);
    await expect(res.body.status).toBe(400);
  });
  test("User NULL", async () => {
    let invalidUser = {};
    console.log(invalidUser);
    const res = await request(app).post("/auth/login").send(invalidUser);
    await expect(res.body.status).toBe(400);
  });
  /* AUTH CONTROLLER ERRORS  FOR EMAIL/PASSWORD/USER NULL TEST - NEEDS TO BE FIXED BEFORE RUNNING, 
    Cannot set headers after they are sent to the clientError [ERR_HTTP_HEADERS_SENT]: 
    Cannot set headers after they are sent to the client     */
});

/* SUCCESS TESTS WITH ERRORS
describe("/organizations/1/public", () => {
  test("probando la organizacion", async () => {
    const res = await request(app).get("/organizations/1/public");
    expect(res.status).toBe(200);
  });
  test("probando FALLAR", async () => {
    const res = await request(app).get("/organizations/15/public");
    expect(res.body.status).toBe(400);
  });
  test("probando el cuerpo", async () => {
    const res = await request(app).get("/organizations/1/public");
    expect(typeof res.body).toBe("object");
  });
}); */
afterAll(async (done) => {
  await db.sequelize.close();
  done();
});
