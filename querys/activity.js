const { Activity } = require("../database");

const createActivity = async ({ name, content }) => {
  Activity.create({
    name: name,
    content: content,
  })
    .then((data) => {
      if (data) return data;
    })
    .catch((err) => {
      if (err) {
        console.log(err);
      }
    });
};

const getActivity = async(id) => {
  return await Activity.findByPk(id);
}

const getActivities = async () => {
  return await Activity.findAll( {attributes: ["id","name", "image", "content", "createdAt"]})
};

const updateActivity = async(id, data) => {
  return await Activity.update(data, { where: { id: id } });
}

const find_and_delete = async (id) => {
  return await Activity.destroy({ where: { id } });
}

module.exports = {
  createActivity,
  getActivity,
  updateActivity,
  getActivities,
  find_and_delete
};
