const { User } = require("../database");

exports.create = async (data) => {
  return await User.create(data);
};

exports.findAll = async () => {
  return await User.findAll({
    order: [["id", "ASC"]],
  });
};

exports.findByPk = async (id) => {
  return await User.findByPk(id);
};

exports.find_and_update = async(id, body) => {
  try {
    const data = await User.update(
      {
        firstName: body.firstName,
        lastName: body.lastName,
        email: body.email,
        roleId: body.roleId,
        organizationId: body.organizationId,
      },
      { returning: true, plain: true, where: { id: id } }
    );
    console.log('QUERY RESPONSE ', data);
    return data[1];
  } catch (error) {
    console.log("hubo un error", error);
    return error;
  }
}

exports.destroy = async (id) => {
  return await User.destroy({ where: { id } });
};
