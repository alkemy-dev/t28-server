const db = require("../models");
async function find_by_id(id) {
  const data = await db.Organization.findByPk(id, {
    include: [
      {
        model: db.SocialMedia,
      },
    ],
  });
  return data;
}
module.exports = { find_by_id };
