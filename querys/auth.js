const { User } = require("../database");
const db = require("../models");

async function find_user_by_email(email) {
  return await User.findOne({ where: { email } });
}

const find_user_role = async (id) => {
  try {
    const user = await db.User.findOne({
      where: { id },
      include: [
        {
          model: db.Role,
          as: "role",
          // through: {

          //   where: {name: 'Admin'}
          // }
        },
      ],
    });
    return user.role.name;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  find_user_by_email,
  find_user_role,
};
