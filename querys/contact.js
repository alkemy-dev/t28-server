const { Contact } = require("../database");

/*
exports.findAllContacts = async () => {
  return await Contact.findAll({
    attributes: ["name", "image", "createdAt"],
    where: { type: "News" },
  });
};
*/

exports.createContacts = async(name, phone, email, message) => {
  return Contact.create({
    name: name,
    phone: phone,
    email: email,
    message: message,
    deletedAt: null
  }).catch((err) => {
    if(err){
      console.log(err);
    }
  });
}

exports.findAllContacts = async () => {
  return await Contact.findAll({
    attributes: ["name", "phone", "email", "message"]
  });
};