const db = require("../models/index");

async function find_all() {
  const data = await db.Members.findAll();
  return data;
}
async function find_by_id(id) {
  const data = await db.Members.findByPk(id);
  return data;
}

async function members_query_post(body) {
  const data = await db.Members.create(
    {
      name: body.name,
      image: body.image,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      attributes: { include: ["name", "image"] },
    }
  );
  return data;
}

async function find_and_update(id, body) {
  try {
    const data = await db.Members.update(
      {
        name: body.name,
        image: body.image,
        updatedAt: new Date(),
      },
      { returning: true, plain: true, where: { id: id } }
    );
    return data;
  } catch (error) {
    console.log("hubo un error", error);
    return error;
  }
}

async function find_and_delete(id) {
  const data = await db.Categories.destroy({
    where: {
      id: id,
    },
  });
  return data;
}
module.exports = {
  find_all,
  find_by_id,
  members_query_post,
  find_and_update,
  find_and_delete
};
