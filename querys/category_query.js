const db = require("../models");

async function find_all() {
  const data = await db.Categories.findAll({ attributes: ["name", "id", "description"] });
  return data;
}
async function find_by_id(id) {
  const data = await db.Categories.findByPk(id);
  return data;
}
async function category_query_post(body) {
  const data = await db.Categories.create(
    {
      name: body.name,
      description: body.description,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      attributes: { include: ["name", "description"] },
    }
  );
  return data;
}

async function find_and_update(id, body) {
  try {
    const data = await db.Categories.update(
      {
        name: body.name,
        description: body.description,
        updatedAt: new Date(),
      },
      { returning: true, plain: true, where: { id: id } }
    );
    return data;
  } catch (error) {
    console.log("hubo un error", error);
    return error;
  }
}

async function find_and_delete(id) {
  const data = await db.Categories.destroy({
    where: {
      id: id,
    },
  });
  return data;
}
module.exports = {
  find_all,
  category_query_post,
  find_by_id,
  find_and_update,
  find_and_delete,
};
