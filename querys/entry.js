const { Entry } = require("../database");

exports.findAllNews = async () => {
  return await Entry.findAll({
    attributes: ["id","name", "image", "content", "categoryId","type","createdAt"],
    where: { type: "News" },
  });
};

exports.createNews = async(name, content, image, categoryID, type) => {
  Entry.create({
    name: name,
    content: content,
    image: image,
    categoryID: categoryID,
    type: type
  }).catch((err) => {
    if(err){
      console.log(err);
    }
  });
};
 
exports.findNewsByID = async (id) => {
  try {
    const data = await Entry.findByPk(id);
    return data;
  } catch (error) {
    return error;
  }
}; 

exports.UpdateNewsByID = async (id, body) => {
  try {
    const data = await Entry.update(
      {
        name: body.name,
        content: body.content,
        image: body.image,
        categoryId: body.categoryId,
        type: body.type,
        deletedAt: new Date(),
        updatedAt: new Date(),
      },
      { returning: true, plain: true, where: { id: id } }
    );
    console.log("la informacion actualizada!", data[1]);
    return data;
  } catch (error) {
    return error;
  }
};

exports.findOne = async (id) => {
  return await Entry.findByPk(id);
};

exports.destroy = async (id) => {
  return await Entry.destroy({ where: { id } });
}
