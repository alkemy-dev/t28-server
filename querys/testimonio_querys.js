const { Testimony } = require("../database/index");

async function find_by_id(id) {
  const data = await Testimony.findByPk(id);
  return data;
}
async function find_and_update(id, body) {
  const data = await Testimony.update(
    {
      name: body.name,
      content: body.content,
      image: body.image,
      updatedAt: new Date(),
    },
    { returning: true, plain: true, where: { id: id } }
  );
  return data;
}
async function find_and_delete(id) {
  const data = await Testimony.destroy({
    where: {
      id: id,
    },
  });
  return data;
}
async function testimony_query_post(body) {
  const data = await Testimony.create(
    {
      name: body.name,
      content: body.content,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      attributes: { include: ["name", "content"] },
    }
  );
  return data;
}


const get_all = async () => {
  return await Testimony.findAll( {attributes: ["id","name", "image", "content", "createdAt"]})
};

module.exports = {
  find_by_id,
  find_and_update,
  testimony_query_post,
  find_and_delete,
  get_all
};

/*
Error card public organization testimony data:
 {
      name: body.name,
      user_id: body.user_id,
      image: body.image,
      phone: body.phone,
      address: body.address,
      welcomeText: body.welcomeText,
      updatedAt: new Date(),
    },*/
