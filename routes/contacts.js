const express = require("express");
const router = express.Router();
const { check } = require('express-validator');
const { verifyJWT } = require("../middlewares/userValidate");
const {
    getAllContacts,
    postContacts
} = require("../controllers/contact");
const {
    GET_CONTACTS,
    POST_CONTACTS,
    CONTACT_NAME_REQUIRED,
    CONTACT_NAME_TYPE,
    CONTACT_EMAIL_REQUIRED,
    CONTACT_EMAIL_TYPE
} = require("../const/index");

router.get(GET_CONTACTS, verifyJWT, getAllContacts);

// create contact
router.post(POST_CONTACTS,
    [
        check('name', CONTACT_NAME_REQUIRED).not().isEmpty(),
        check('name', CONTACT_NAME_TYPE).isString(),
        check('email', CONTACT_EMAIL_REQUIRED).not().isEmpty(),
        check('email', CONTACT_EMAIL_TYPE).isEmail()
    ],
    postContacts);

module.exports = router;

