const express = require("express");
const router = express.Router();
const { USER_REGISTER, GET_USERS, DELETE_USER } = require("../const/index");
const {
  userRegister,
  getUsers,
  userDelete,
  findUsers,
  userUpdate,
} = require("../controllers/user");
const { login } = require("../controllers/auth_controller");
const {
  userValidate,
  validate,
  verifyJWT,
} = require("../middlewares/userValidate");
const { User } = require("../database");
const { RequestValidation } = require("../middlewares/auth/index");

/* GET users listing. */
router.get(GET_USERS, getUsers);

/**Test user is Admin */
router.get("/getUser/:id", RequestValidation, findUsers);

// GET Authenticate:
router.get("/authUser", verifyJWT, (req, res) => {
  if (req.headers && req.headers.authorization) {
    var authorization = req.headers.authorization.split(" ")[1],
      decoded;
    try {
      decoded = jwt.verify(authorization, secret.secretToken);
    } catch (e) {
      return res.status(401).send("unauthorized");
    }
    var userId = decoded.id;
    User.findOne({ _id: userId }).then(function (user) {
      return res.send(200);
    });
  }
  return res.send(500);
});

/* POST user register with auto login registration */
router.post(USER_REGISTER, userValidate(), validate, userRegister);

/* UPDATE USER */
router.put("/:id", userValidate(), userUpdate);

/* DELETE user */
router.delete(DELETE_USER, userDelete);

module.exports = router;
