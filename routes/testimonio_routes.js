const testimonioRoutes = require("express").Router();
const {
  testimony_find,
  testimony_update,
  testimony_post,
  testimony_delete,
  testimonies_get,
} = require("../controllers/testimonio_controller");
const {
  testimony_validation_post,
} = require("../middlewares/testimonyValidations");

/*get*/
testimonioRoutes.get("/:id", testimony_find);

testimonioRoutes.get("/", testimonies_get);
/*post*/
testimonioRoutes.post("/", testimony_validation_post, testimony_post);
/*update */
testimonioRoutes.put(
  "/:id",
  testimony_validation_post,
  testimony_update,
  testimony_find
);
/*delete */
testimonioRoutes.delete("/:id", testimony_delete);

module.exports = testimonioRoutes;
