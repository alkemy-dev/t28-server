const express = require("express");
const router = express.Router();
const { 
    activity_post, 
    activity_get, 
    activity_update, 
    activities_get,
    activity_delete
} = require("../controllers/activity_controller");
const { activityValidationPost, activityValidationUpdate } = require("../middlewares/activityValidate");
const { POST_ACTIVITY, GET_ACTIVITY, UPDATE_ACTIVITY, GET_ACTIVITIES, DELETE_ACTIVITY } = require("../const/index");

/* POST Activity */
router.post(POST_ACTIVITY, activityValidationPost, activity_post);

/* GET Activity by id */
router.get(GET_ACTIVITY, activity_get);

router.get(GET_ACTIVITIES, activities_get);

/* UPDATE Activity */
router.put(UPDATE_ACTIVITY, activityValidationUpdate, activity_update);
/*DELETE */
router.delete(DELETE_ACTIVITY, activity_delete);

module.exports = router;