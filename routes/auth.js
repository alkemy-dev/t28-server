const express = require('express');
const router = express.Router();
const { login } = require('../controllers/auth_controller');
const { postLoginValidation } = require('../middlewares/auth/index');
/* Post login. */
router.post('/login', postLoginValidation, login);

module.exports = router;