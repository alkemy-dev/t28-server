const organizationRoutes = require("express").Router();
const { organization_find } = require("../controllers/organization_controller");

/*GET */
organizationRoutes.get("/:id/public", organization_find);

module.exports = organizationRoutes;
