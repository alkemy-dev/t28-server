const express = require("express");
const router = express.Router();
const { getNews, updateNews, getNewsByID, getEntryById, postNews, deleteNews } = require("../controllers/entry");
const { GET_NEWS, GET_ENTRY, POST_NEWS, DELETE_NEWS, UPDATE_NEWS } = require("../const/index");

/* GET news listing. */
router.get(GET_NEWS, getNews);
//UPDATE and SHOW news
router.put("/:id", updateNews, getNewsByID);

/* GET news listing. */
router.get(GET_NEWS, getNews);
router.get(GET_ENTRY, getEntryById);
router.post(POST_NEWS, postNews);

// Delete news
router.delete(DELETE_NEWS, deleteNews);

// get news listing
router.get(GET_NEWS, getNews);

// get entry by id
router.get(GET_ENTRY, getEntryById);

// create entry
router.post(POST_NEWS, postNews);

// update and show news
router.put(UPDATE_NEWS, updateNews, getNewsByID);

// delete news
router.delete(DELETE_NEWS , deleteNews)

module.exports = router;
