const membersRoutes = require("express").Router();
const {
  members_find_by_id,
  members_post,
  members_update,
  members_delete,
  members_find_all,
} = require("../controllers/members_controller");
const { members_validation } = require("../middlewares/membersValidate");

/*GET */
membersRoutes.get("/", members_find_all);
membersRoutes.delete("/:id", members_delete);
membersRoutes.post("/", members_validation, members_post);
membersRoutes.put(
  "/:id",
  members_validation,
  members_update,
  members_find_by_id
);

module.exports = membersRoutes;
