//categories

const categoryRoutes = require("express").Router();
const {
  category_get_all,
  category_post,
  category_find_by_id,
  category_update,
  category_delete,
} = require("../controllers/category_controller");
const { category_validation_post } = require("../middlewares/categoryValidate");

/*GET */
categoryRoutes.get("/", category_get_all);

/*POST*/
categoryRoutes.post("/", category_validation_post, category_post);

/*PUT */
categoryRoutes.put(
  "/:id",
  category_validation_post,
  category_update,
  category_find_by_id
);
/*DELETE */
categoryRoutes.delete("/:id", category_delete);
module.exports = categoryRoutes;
