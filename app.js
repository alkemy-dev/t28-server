const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const fileUpload = require("express-fileupload");
const logger = require("morgan");
const cors = require("cors");
require("dotenv").config();

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const authRoutes = require("./routes/auth");
const entriesRoutes = require("./routes/entries");
const contactsRoutes = require("./routes/contacts");
const activitiesRoutes = require("./routes/activities");
const testimonioRoutes = require("./routes/testimonio_routes");
const organizationRoutes = require("./routes/organization");
const categoryRoutes = require("./routes/category");
const membersRoutes = require("./routes/members");

const app = express();
app.use(cors());

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(fileUpload());

app.use("/", indexRouter);
app.use("/organizations", organizationRoutes);
app.use("/testimonials", testimonioRoutes);
app.use("/members", membersRoutes);
app.use("/categories", categoryRoutes);
app.use("/users", usersRouter);
app.use("/auth", authRoutes);
app.use("/news", entriesRoutes);
app.use("/activities", activitiesRoutes);
app.use("/contacts", contactsRoutes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);

  res.json({
    status: err.status,
    message: err.message,
  });
});

module.exports = app;
