module.exports = {
  // url page
  URL: "https://somosmas.com",
  URL_CONTACT: "https://somosmas.com/contacto",
  // http response codes
  SUCCESS_CODE: 200,
  ERROR_CLIENT_CODE: 400,
  ERROR_SERVER_CODE: 500,
  // form messages
  SUCCESS_MESSAGE: "INFORMACION ENCONTRADA",
  SUCCESS_DELETE_MESSAGE: "SE BORRO LA INFORMACION CORRECTAMENTE",
  ERROR_GET_MESSAGE: "ERROR AL BUSCAR LA INFORMACION",
  ERROR_POST_MESSAGE: "ERROR AL ENVIAR LA INFORMACION",
  ERROR_EMPTY_DATA_MESSAGE: "ERROR, COMPLETE TODOS LOS CAMPOS",
  ERROR_NOT_TYPE_STRING_MESSAGE: "ERROR, DEBE COMPLETAR EL CAMPO CON TEXTO",
  ERROR_DATA_NOT_FOUND_MESSAGE: "NO SE ENCONTRO LA INFORMACION SOLICITADA",
  ERROR_AUTHENTICATION_FAILED:
    "Authentication failed email/password is not correct!",
  // sendgrid API mail service messaged
  EMAIL_DEFAULT_SUBJECT: "Agradecimiento de SOMOS MÁS",
  EMAIL_DEFAULT_TEXT: "¡GRACIAS POR CONTACTARSE CON NOSOTROS!",
  EMAIL_DEFAULT_HTML: "<strong>EQUIPO DE LA ONG</strong>",
  EMAIL_SENT_SUCCESFULLY: "Mail sent correctly",
  EMAIL_SENT_ERROR: "An ERROR ocurred trying to send the email",
  // JSON Web Token messages
  TOKEN_MESSAGE_SUCCESS: "You need token",
  TOKEN_MESSAGE_FAIL: "Failed to authenticate",
  SECRET_WORD: "secret",
  AUTHORIZATION: "authorization",
  ERROR_VALIDATION: "Validation Erros",
  TTL: "1d",
  // API messages
  ERROR_USER_DELETE_MESSAGE: "USUARIO NO ENCONTRADO",
  SUCCESS_USER_DELETE_MESSAGE: "USUARIO ELIMINADO",
  ERROR_SERVER: "ERROR EN EL SERVIDOR",
  ERROR_NEWS_DELETE_MESSAGE: "No se encuentra la novedad",
  SUCCESS_NEWS_DELETE_MESSAGE: "La novedad se elimino correctamente",
  ADMIN_ROLE: 'Admin',
  ERROR_USER_WITHOUT_PERMISSIONS: 'User does not have permissions to access',
  ERROR_ACTIVITY_DELETE_MESSAGE: "No se encuentra la actividad",
  SUCCESS_ACTIVITY_DELETE_MESSAGE: "La actividad se elimino correctamente"
};
