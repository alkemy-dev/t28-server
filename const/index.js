const errorMinCharacters = (character) => {
  return `Este campo debe tener mínimo ${character} carácteres`;
};

module.exports = {
  /*****  ROUTES for request  *****/

  // POST: user register
  USER_REGISTER: "/auth/register",
  // LOGIN User
  USER_LOGIN: "/auth/login",
  // GET: get all users
  GET_USERS: "/",
  // DELETE: delete user :id
  DELETE_USER: "/:id",
  // GET: get all news
  GET_NEWS: "/",
  //POST: add news
  POST_NEWS: "/",
  // PUT: update and show news
  UPDATE_NEWS: "/:id",
  // GET: get entry by id
  GET_ENTRY: "/:id",
  // DELETE: delete news
  DELETE_NEWS: "/:id",
  // GET: all contacts
  GET_CONTACTS: "/",
  // POST: add contact
  POST_CONTACTS: "/",
  // GET: get activity by id
  GET_ACTIVITY: "/:id",
  // GET: get activities
  GET_ACTIVITIES: '/',
  // PUT: update activity
  UPDATE_ACTIVITY: "/:id",
  // DELETE: delete ACTIVITY
  DELETE_ACTIVITY: "/:id",

  // Validations  
  CONTACT_NAME_REQUIRED: 'El nombre del contacto es obligatorio',
  CONTACT_NAME_TYPE: 'El nombre del contacto debe ser String',
  CONTACT_EMAIL_REQUIRED: 'El email del contacto es obligatorio',
  CONTACT_EMAIL_TYPE: 'El formato de email del contacto no es válido',
  
  // -- Activities -- ///
  // POST: Create Activity
  POST_ACTIVITY: "/",

  /***** ROUTES for tests *****/

  // TEST: get acitvities
  TEST_GET_ACTIVITIES: '/activities',
  // TEST: get activity by id
  TEST_GET_ACTIVITY: '/activities/1',
  // TEST: post acitvity
  TEST_POST_ACTIVITY: '/activities',
  // TEST: updated activity
  TEST_UPDATE_ACTIVITY: '/activities/1',

  /*****  ERROR messages  *****/

  // error: server
  ERROR_SERVER: "Error al buscar información",
  // error: field required
  ERROR_NOT_EMPTY: "Este campo es requerido",
  // error: email
  ERROR_EMAIL: "Este campo debe ser un correo",
  // error: min characters
  errorMinCharacters,
  ERROR_USER_LOGIN: 'Error, el usuario no está logueado',
  ERROR_USER_INVALID: 'Error, no existe el usuario',
  ERROR_USER_ADMIN: 'Error, el usuario debe ser un administrador',

  /***** AMAZON s3 *****/

  BUCKET_NAME: "alkemy-ong",

  /***** SUCCESS messages *****/

  SUCCESS_UPDATE_ACTIVITY: "Actividad modificada correctamente"
};