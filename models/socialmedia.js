"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SocialMedia extends Model {
    static associate(models) {
      SocialMedia.belongsTo(models.Organization, {
        foreignKey: {
          name: "organizationId",
          allowNull: false,
        },
        targetKey: "id",
      });
    }
  }
  SocialMedia.init(
    {
      facebook: DataTypes.STRING,
      linkedin: DataTypes.STRING,
      instagram: DataTypes.STRING,
      organizationId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "SocialMedia",
      freezeTableName: true,
    }
  );
  return SocialMedia;
};
