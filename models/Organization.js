"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Organization extends Model {
    static associate(models) {
      Organization.hasOne(models.SocialMedia, {        
        foreignKey: {
          name: "organizationId",
          as: 'organization'          
        },
      });
    }
  }
  Organization.init(
    {
      name: DataTypes.STRING,
      image: DataTypes.STRING,
      phone: DataTypes.STRING,
      address: DataTypes.STRING,
      wellcomeText: DataTypes.STRING,
    },

    {
      sequelize,
      modelName: "Organization",
      freezeTableName: true,
    }
  );
  return Organization;
};
