"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Slide extends Model {
    static associate(models) {
    }
  }
  Slide.init(
    {
      imageUrl: DataTypes.STRING,
      text: DataTypes.STRING,
      order: DataTypes.INTEGER,
      organizationId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Slide",
    }
  );
  return Slide;
};
