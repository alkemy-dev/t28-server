const db = require("../models");
const userModel = require("../models/user");
const entryModel = require("../models/entry");
const activityModel = require("../models/activity");
const contactModel = require("../models/contact");
const organizationModel = require("../models/Organization");
const testimony = require("../models/testimony");

const User = userModel(db.sequelize, db.Sequelize);
const Entry = entryModel(db.sequelize, db.Sequelize);
const Activity = activityModel(db.sequelize, db.Sequelize);
const Contact = contactModel(db.sequelize, db.Sequelize);
const Organization = organizationModel(db.sequelize, db.Sequelize);
const Testimony = testimony(db.sequelize, db.Sequelize);

// async db
db.sequelize
  .sync({ force: false })
  .then(console.log("Correct async"))
  .catch((e) => console.log("ERROR  :  ", e));

module.exports = {
  User,
  Entry,
  Activity,
  Contact,
  Organization,
  Testimony,
};
