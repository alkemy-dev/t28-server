const AWS = require('aws-sdk');
const config = require('../config/config').development;
const { BUCKET_NAME } = require('../const');

// config
const s3 = new AWS.S3({
    accessKeyId: config.aws_access_key_id,
    secretAccessKey: config.aws_secret_access_key
});

// Get file
exports.getFile = async(file) => {        
    return `https://${BUCKET_NAME}.s3.amazonaws.com/ong-team-28/${file}`;
}

// Upload File
exports.uploadFile = async(file) => {        
    const params = {
        Bucket: BUCKET_NAME,
        Key: `ong-team-28/${file.file.img.name}`,
        Body: file.file.img.data
    }

    return s3.putObject(params).promise();
}

// Delete file
exports.deleteFile = async(file) => {      
    const params = {
        Bucket: BUCKET_NAME,
        Key: `ong-team-28/${file}`
    }

    return s3.deleteObject(params).promise();
}