const {
  URL,
  URL_CONTACT,
  EMAIL_DEFAULT_SUBJECT,
  EMAIL_DEFAULT_TEXT,    
  EMAIL_SENT_SUCCESFULLY,
  EMAIL_SENT_ERROR
}
= require('../const/const');
// Integrate sendgrid API to be able to send messages to users when they enter their email's on the registration form
const sgMail = require('@sendgrid/mail');
const Hogan = require('hogan.js');
const fs = require('fs');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const mailingService = {};
  
mailingService.sendMail = (req, res) => {    
  const template = fs.readFileSync('./template/sendMailContact/index.hjs', 'utf-8');
  const compiledTemplate = Hogan.compile(template);
  // create msg
  const msg = {
    to: req.body.email,
    from: process.env.SENDGRID_API_MAIL, // Use the email address or domain you verified above
    subject: EMAIL_DEFAULT_SUBJECT,
    text: EMAIL_DEFAULT_TEXT,
    html: compiledTemplate.render({name: req.body.name, url: URL, urlContact: URL_CONTACT})
  };  
  sgMail
    .send(msg)
    .then((response) => {console.log(EMAIL_SENT_SUCCESFULLY)},
      error => {
        console.error('ERROR : ',error);
        // sent error msg and array with error the errors details
        if (error.response) {
          res.json({EMAIL_SENT_ERROR, 'Detail: ': error.response.body});
        }
    });
}


module.exports = mailingService;