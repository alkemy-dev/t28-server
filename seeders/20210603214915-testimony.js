'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Testimonies', [
      {
        name: 'Julio',
		    image: 'https://i.ytimg.com/vi/ibSlP4-1ZlY/maxresdefault.jpg',
		    content: 'Este es un testimonio del mes de Julio',
        createdAt: new Date,
        updatedAt: new Date
      },
      {
        name: 'Agosto',
		    image: 'https://th.bing.com/th/id/R7a75a50b03f6d2cde848400c0f115004?rik=FtlPJM4RvbW8AA&riu=http%3a%2f%2f4.bp.blogspot.com%2f_Puej9HEkwgI%2fS8P6_z7rbHI%2fAAAAAAAAIOI%2fJIkveiHOcDE%2fs400%2fDSCN0181.JPG&ehk=K9ql8L4caaxq9DFX9Wb%2fMX8Bl1PqejRs07fwDJ8m2%2fU%3d&risl=&pid=ImgRaw',
		    content: 'Este es un testimonio de Agosto',
        createdAt: new Date,
        updatedAt: new Date
      },
      {
        name: 'Junio',
		    image: 'https://images.charentelibre.fr/2019/08/09/5d4dad607971bbaf400d2e8d/golden/leo-paul-12-ans-s.jpg',
		    content: 'Este testimonio llegó unos meses tarde',
        createdAt: new Date,
        updatedAt: new Date
      }      
    ], {});
  },

  

  down: async (queryInterface, Sequelize) => {
    
     await queryInterface.bulkDelete('Testimonies', null, {});
  }
};