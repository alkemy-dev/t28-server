"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "Entries",
      [
        {
          name: "Novedad 1",
          content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda excepturi repellendus, temporibus deleniti laudantium tenetur repellat neque nostrum dolor, dolore explicabo alias. Placeat aliquam nesciunt nulla exercitationem maiores fugiat in?",
          image:
            "https://lh3.google.com/u/0/d/1zKUKLGufURBQivbTnvlgn17TZyzk8DTI=w1920-h942-iv2",
          
          type: "News",
          deletedAt: new Date(),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Novedad 2",
          content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda excepturi repellendus, temporibus deleniti laudantium tenetur repellat neque nostrum dolor, dolore explicabo alias. Placeat aliquam nesciunt nulla exercitationem maiores fugiat in?",
          image:
            "https://lh3.google.com/u/0/d/1ileZuq6dMphx9i-aFtz95iSDRsRVZgLj=w1448-h942-iv1",
         
          type: "News",
          deletedAt: new Date(),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Novedad 3",
          content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda excepturi repellendus, temporibus deleniti laudantium tenetur repellat neque nostrum dolor, dolore explicabo alias. Placeat aliquam nesciunt nulla exercitationem maiores fugiat in?",
          image:
            "https://lh3.google.com/u/0/d/10zBKAJOJLjMKXWMXDad_SUg8yL5znm91=w1448-h942-iv1",
          
          type: "News",
          deletedAt: new Date(),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Novedad 4",
          content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda excepturi repellendus, temporibus deleniti laudantium tenetur repellat neque nostrum dolor, dolore explicabo alias. Placeat aliquam nesciunt nulla exercitationem maiores fugiat in?",
          image:
            "https://lh3.google.com/u/0/d/1VogDd65eOHEIt-hyRpd-ysDf0SfTdWfs=w1448-h942-iv1",
          
          type: "News",
          deletedAt: new Date(),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Novedad 5",
          content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda excepturi repellendus, temporibus deleniti laudantium tenetur repellat neque nostrum dolor, dolore explicabo alias. Placeat aliquam nesciunt nulla exercitationem maiores fugiat in?",
          image:
            "https://lh3.google.com/u/0/d/1yqHbCDAx6FFbN1mRj0dCvEkjeqSsov2u=w1448-h942-iv2",
          
          type: "News",
          deletedAt: new Date(),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Novedad 6",
          content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda excepturi repellendus, temporibus deleniti laudantium tenetur repellat neque nostrum dolor, dolore explicabo alias. Placeat aliquam nesciunt nulla exercitationem maiores fugiat in?",
          image:
            "https://lh3.google.com/u/0/d/1EfWmYnA2R49ZW8anbMT9Hg2J02pVT5D9=w1448-h942-iv2",
          
          type: "News",
          deletedAt: new Date(),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Novedad 7",
          content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda excepturi repellendus, temporibus deleniti laudantium tenetur repellat neque nostrum dolor, dolore explicabo alias. Placeat aliquam nesciunt nulla exercitationem maiores fugiat in?",
          image:
            "https://lh3.google.com/u/0/d/1tBkFZIYz2Y1Tc1WIqKXS_H3YpZOSYDQJ=w1448-h942-iv2",
         
          type: "News",
          deletedAt: new Date(),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Novedad 8",
          content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda excepturi repellendus, temporibus deleniti laudantium tenetur repellat neque nostrum dolor, dolore explicabo alias. Placeat aliquam nesciunt nulla exercitationem maiores fugiat in?",
          image:
            "https://lh3.google.com/u/0/d/1S1AurINBtoSmI8qEBxhrXAW9m34rsF5p=w1448-h942-iv2",
         
          type: "News",
          deletedAt: new Date(),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
