'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Activities', [
      {
        name: 'Tarde de futbol en el potrero!',
        image: 'https://th.bing.com/th/id/Re183520fa4906765505a896069b2c1cf?rik=idsqN3bl9rzRCA&riu=http%3a%2f%2fwww.diario21.tv%2fnotix2%2fmultimedia%2fimagenes%2ffotos%2f2017-11-29%2f251422.jpg&ehk=%2fcoozbvXbPsZojrxHKE8dxLSXSlGw2e%2btJC%2bv0jS38A%3d&risl=&pid=ImgRaw',
        content: 'Los mas chiquitos juegan al futbol',
        createdAt: new Date,
        updatedAt: new Date
      },
      {
        name: 'Bici Club',
        image: 'https://biciclub.com/wp-content/uploads/2020/07/MG_9281.jpg',
        content: 'Evento en conjunto con ong de BA',
        createdAt: new Date,
        updatedAt: new Date
      },
      {
        name: 'Comedor comunitario',
        image: 'https://www.abc.com.py/resizer/gdFoSDyHEL0yxEjJO1aPAbDUB_w=/fit-in/770x495/smart/arc-anglerfish-arc2-prod-abccolor.s3.amazonaws.com/public/KTSYDP6VKVBJ5LPSEBEJRLJLMA.jpg',
        content: 'Panza llena, corazón contento',
        createdAt: new Date,
        updatedAt: new Date
      }      
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    
     await queryInterface.bulkDelete('Activities', null, {});
  }
};