'use strict';
let usersFake = [];
    for (let i = 0; i < 20; i++) {
      const roleName = i < 10 ? 1 : 2;
      usersFake.push({
        firstName: `Usuario ${i}`,
        lastName: `User ${i}`,
        email: `a@a${i}.com`,
        password: "1234567",
        organizationId: 1,
        roleId: roleName,
        image:
          "https://media.istockphoto.com/photos/portrait-of-happy-indian-man-smiling-at-home-picture-id1270067432",
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }
  module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    
    await queryInterface.bulkInsert('Users',usersFake, {});
    
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
