"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const info = [
      {
        name: "Laguna",
        image: "www.lagunaong.com/laguna.png",
        phone: "12345677899",
        address: "calle falsa 1234",
        wellcomeText: "bienvenidos a la ong!!!!!!",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];
    await queryInterface.bulkInsert("Organization", info, {});
  },

  down: async (queryInterface, Sequelize) => {
    /*  await queryInterface.bulkDelete("Organization", null, {}); */
  },
};
