'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Categories', [{
        name: 'First Sample Category',
        description: 'Categories seed built for development porpuses',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Second Sample Category',
        description: 'Development Seed for Categories model type',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Third Sample Category',
        description: 'Are looking for another fake Category?',
        createdAt: new Date(),
        updatedAt: new Date()
      }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Categories', null, {});
  }
};
