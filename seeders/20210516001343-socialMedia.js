"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const info = [
      {
        facebook: "face laguna 1",
        linkedin: "insta laguna 1",
        instagram: "insta laguna 1",
        organizationId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        facebook: "face laguna 2",
        linkedin: "insta laguna 2",
        instagram: "insta laguna 2",
        organizationId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];
    await queryInterface.bulkInsert("SocialMedia", info, {});
  },

  down: async (queryInterface, Sequelize) => {
    /*  await queryInterface.bulkDelete("SocialMedia", null, {}); */
  },
};
