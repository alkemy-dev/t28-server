const {
  SUCCESS_CODE,
  SUCCESS_MESSAGE,
  ERROR_GET_MESSAGE,
  ERROR_DATA_NOT_FOUND_MESSAGE,
  ERROR_CLIENT_CODE,
  ERROR_SERVER_CODE,
  ERROR_SERVER,
  SUCCESS_DELETE_MESSAGE,
} = require("../const/const");
const {
  find_all,
  category_query_post,
  find_by_id,
  find_and_update,
  find_and_delete,
} = require("../querys/category_query");

/* FIND BY ID */
async function category_find_by_id(req, res) {
  const id = req.params.id;
  try {
    const data = await find_by_id(id);

    if (data !== null) {
      res.json({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: data,
      });
    } else {
      res.json({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE,
        data: data,
      });
    }
  } catch (error) {
    res.json({
      status: ERROR_CLIENT_CODE,
      message: ERROR_GET_MESSAGE,
      data: error,
    });
  }
}

/* FIND ALL */
async function category_get_all(req, res) {
  try {
    const data = await find_all();
    if (data !== null) {
      res.json({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: data,
      });
    } else {
      res.status(400).json({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE,
        data: data,
      });
    }
  } catch (error) {
    res.status(500).json({
      status: ERROR_SERVER_CODE,
      message: ERROR_GET_MESSAGE,
      data: error,
    });
  }
}
/* POST */
async function category_post(req, res) {
  try {
    const data = await category_query_post(req.body);
    if (data !== null) {
      res.json({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: data,
      });
    } else {
      res.status(ERROR_CLIENT_CODE).json({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE,
        data: data,
      });
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
}

/* PUT */
async function category_update(req, res, next) {
  const id = req.params.id;
  try {
    const data = await find_and_update(id, req.body);
    if (!data[1]) {
      res
        .status(ERROR_CLIENT_CODE)
        .json({ message: ERROR_DATA_NOT_FOUND_MESSAGE, data: data });
    } else {
      next();
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
}

async function category_delete(req, res) {
  const id = req.params.id;
  try {
    const data = await find_and_delete(id);

    if (data !== 1) {
      res
        .status(ERROR_CLIENT_CODE)
        .json({ message: ERROR_DATA_NOT_FOUND_MESSAGE, data: data });
    } else {
      res
        .status(SUCCESS_CODE)
        .json({ message: SUCCESS_DELETE_MESSAGE, data: data });
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
}
module.exports = {
  category_get_all,
  category_post,
  category_find_by_id,
  category_update,
  category_delete,
};
