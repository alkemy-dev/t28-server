const {
  SUCCESS_CODE,
  SUCCESS_MESSAGE,
  SUCCESS_DELETE_MESSAGE,
  ERROR_GET_MESSAGE,
  ERROR_DATA_NOT_FOUND_MESSAGE,
  ERROR_CLIENT_CODE,
} = require("../const/const");
const {
  find_all,
  members_query_post,
  find_and_update,
  find_by_id,
  find_and_delete
} = require("../querys/members_query");

/* CONTROLLER GET */
async function members_find_all(req, res) {
  try {
    const data = await find_all();

    if (data !== null) {
      res.json({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: data,
      });
    } else {
      res.json({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE,
        data: data,
      });
    }
  } catch (error) {
    res.json({
      status: ERROR_CLIENT_CODE,
      message: ERROR_GET_MESSAGE,
      data: error,
    });
  }
}

/* FIND BY ID */
async function members_find_by_id(req, res) {
  const id = req.params.id;
  try {
    const data = await find_by_id(id);

    if (data !== null) {
      res.json({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: data,
      });
    } else {
      res.json({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE,
        data: data,
      });
    }
  } catch (error) {
    res.json({
      status: ERROR_CLIENT_CODE,
      message: ERROR_GET_MESSAGE,
      data: error,
    });
  }
}

// CONTROLLER POST
async function members_post(req, res) {
  try {
    const data = await members_query_post(req.body);
    if (data !== null) {
      res.json({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: data,
      });
    } else {
      res.json({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE,
        data: data,
      });
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
}

// CONTROLLER UPDATE
async function members_update(req, res, next) {
  const id = req.params.id;
  try {
    const data = await find_and_update(id, req.body);

    if (!data[1]) {
      res
        .status(ERROR_CLIENT_CODE)
        .json({ message: ERROR_DATA_NOT_FOUND_MESSAGE, data: data });
    } else {
      next();
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
}
/*
  // CONTROLLER DELETE */
async function members_delete(req, res) {
  const id = req.params.id;
  try {
    const data = await find_and_delete(id);

    if (data !== 1) {
      res
        .status(ERROR_CLIENT_CODE)
        .json({ message: ERROR_DATA_NOT_FOUND_MESSAGE, data: data });
    } else {
      res
        .status(SUCCESS_CODE)
        .json({ message: SUCCESS_DELETE_MESSAGE, data: data });
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
}

module.exports = {
  members_find_all,
  members_find_by_id,
  members_post,
  members_update,
  members_delete
};
