const {
  SUCCESS_CODE,
  SUCCESS_MESSAGE,
  SUCCESS_DELETE_MESSAGE,
  ERROR_GET_MESSAGE,
  ERROR_DATA_NOT_FOUND_MESSAGE,
  ERROR_CLIENT_CODE,
} = require("../const/const");
const {
  find_by_id,
  find_and_update,
  testimony_query_post,
  find_and_delete,
  get_all
} = require("../querys/testimonio_querys");

/* CONTROLLER GET */
async function testimony_find(req, res) {
  const id = req.params.id;
  try {
    const data = await find_by_id(id);

    if (data !== null) {
      res.json({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: data,
      });
    } else {
      res.json({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE,
        data: data,
      });
    }
  } catch (error) {
    res.json({
      status: ERROR_CLIENT_CODE,
      message: ERROR_GET_MESSAGE,
      data: error,
    });
  }
}

const testimonies_get = async(req, res) => {
  try {
    const testimonies = await get_all();

    if(testimonies){
      res.status(SUCCESS_CODE).send({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: testimonies,
        
      })
    }else{
      res.status(ERROR_CLIENT_CODE).send({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE, 
        data: testimonies       
      })
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).send({
      message: ERROR_SERVER,
      data: error
    })
  }
}


/* CONTROLLER POST */
async function testimony_post(req, res) {
  try {
    const data = await testimony_query_post(req.body);
    if (data !== null) {
      res.json({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: data,
      });
    } else {
      res.json({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE,
        data: data,
      });
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
}

/* CONTROLLER UPDATE */
async function testimony_update(req, res, next) {
  const id = req.params.id;
  try {
    const data = await find_and_update(id, req.body);

    if (!data[1]) {
      res
        .status(ERROR_CLIENT_CODE)
        .json({ message: ERROR_DATA_NOT_FOUND_MESSAGE, data: data });
    } else {
      next();
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
}

/* CONTROLLER DELETE */
async function testimony_delete(req, res) {
  const id = req.params.id;
  try {
    const data = await find_and_delete(id);

    if (data !== 1) {
      res
        .status(ERROR_CLIENT_CODE)
        .json({ message: ERROR_DATA_NOT_FOUND_MESSAGE, data: data });
    } else {
      res
        .status(SUCCESS_CODE)
        .json({ message: SUCCESS_DELETE_MESSAGE, data: data });
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
}
module.exports = {
  testimony_find,
  testimony_update,
  testimony_post,
  testimony_delete,
  testimonies_get
};
