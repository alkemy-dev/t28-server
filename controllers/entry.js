const {
  findAllNews,
  findOne,
  UpdateNewsByID,
  findNewsByID,
  destroy,
  createNews,
} = require("../querys/entry");
const {
  ERROR_SERVER,
  ERROR_CLIENT_CODE,
  ERROR_DATA_NOT_FOUND_MESSAGE,
  SUCCESS_CODE,
  SUCCESS_MESSAGE,
  ERROR_SERVER_CODE,
  ERROR_NEWS_DELETE_MESSAGE,
  SUCCESS_NEWS_DELETE_MESSAGE,
} = require("../const/const");

exports.getNews = async (req, res) => {
  try {
    const entries = await findAllNews();

    if (entries) {
      res.json({ entries });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: ERROR_SERVER,
    });
  }
};

exports.updateNews = async (req, res, next) => {
  try {
    const news = await UpdateNewsByID(req.params.id, req.body);
    console.log("la info devuelta es: ", news[1]);
    if (!news[1]) {
      res
        .status(ERROR_CLIENT_CODE)
        .json({ message: ERROR_DATA_NOT_FOUND_MESSAGE, data: news });
    } else {
      next();
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
};

exports.getNewsByID = async (req, res) => {
  try {
    const news = await findNewsByID(req.params.id);
    if (!news.dataValues) {
      res
        .status(ERROR_CLIENT_CODE)
        .json({ message: ERROR_DATA_NOT_FOUND_MESSAGE, data: news });
    }
    res
      .status(SUCCESS_CODE)
      .json({ message: SUCCESS_MESSAGE, data: news.dataValues });
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
};

exports.getEntryById = async (req, res) => {
  const { id } = req.params;
  try {
    const entry = await findOne(id);

    if (entry) {
      res.json({ entry });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: ERROR_SERVER,
    });
  }
};

exports.postNews = async (req, res) => {
  const { name, content, image, categoryID, type } = req.body;
  try {
    const insertNews = await createNews(name, content, image, categoryID, type);
    if (insertNews) {
      res.json({ insertNews });
    }
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: ERROR_SERVER,
    });
  }
};

exports.deleteNews = async (req, res) => {
  try { 
      let news = await findOne(req.params.id);
        
      if (news == null) {
          return res.status(404).json({ msg: ERROR_NEWS_DELETE_MESSAGE })
      }

    await destroy(req.params.id);

    res.json({ message: SUCCESS_NEWS_DELETE_MESSAGE });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: ERROR_SERVER,
    });
  }
}
