const {
  SUCCESS_CODE,
  SUCCESS_MESSAGE,
  ERROR_GET_MESSAGE,
  ERROR_DATA_NOT_FOUND_MESSAGE,
  ERROR_CLIENT_CODE,
  ERROR_SERVER_CODE,
} = require("../const/const");
const { find_by_id } = require("../querys/organization_querys");
async function organization_find(req, res) {
  const id = req.params.id;
  try {
    const data = await find_by_id(id);
    if (data !== null) {
      res.json({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: data,
      });
    } else {
      res.json({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE,
        data: data,
      });
    }
  } catch (error) {
    res.json({
      status: ERROR_SERVER_CODE,
      message: ERROR_GET_MESSAGE,
      data: error,
    });
  }
}

module.exports = {
  organization_find,
};
