const { createContacts, findAllContacts } = require("../querys/contact");
const { ERROR_SERVER, ERROR_USER_LOGIN, ERROR_USER_INVALID, ERROR_USER_ADMIN } = require("../const/const");
const { validationResult } = require('express-validator');
const { findByPk } = require("../querys/user");
const { sendMail } = require('../helpers/mailing.service');

exports.postContacts = async(req, res) => {
  const errores = validationResult(req);
  if ( !errores.isEmpty() ) {
      return res.status(400).json({errores: errores.array() })
  }
  
  const { name, phone, email, message } = req.body;
  try {
    const insertContact = await createContacts(name, phone, email, message);
    if (insertContact) {
      sendMail(req);
      res.json({ insertContact });
    }
  } catch (error) {
      console.log(error);
      res.status(500).send({
        message: ERROR_SERVER,
      });
  }
};

exports.getAllContacts = async (req, res) => {

  const userId = req.userId;
  if (!userId) {
    return res.status(403).json({ error: ERROR_USER_LOGIN });
  }

  try {

    const user = await findByPk(userId);
    if (!user) {
      return res.status(403).json({ error: ERROR_USER_INVALID });
    }

    if (user.roleId !== 1) {
      return res.status(403).json({ error: ERROR_USER_ADMIN });
    }

    // GET ALL CONTACTS
    const contacts = await findAllContacts();
    return res.status(200).json({ contacts });
    
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: ERROR_SERVER,
    });
  }

}