const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { find_user_by_email } = require("../querys/auth");
const {
  ERROR_CLIENT_CODE,
  ERROR_SERVER_CODE,
  ERROR_AUTHENTICATION_FAILED,
  SECRET_WORD,
  TTL,
} = require("../const/const");

const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    const user = await find_user_by_email(email);

    if (!user) {
      res.json({
        error: ERROR_AUTHENTICATION_FAILED,
        status: ERROR_CLIENT_CODE,
      });
    }
    //validation password
    const validPassword = user.password === password; //await bcrypt.compare(password, user.password);

    if (!validPassword) {
      res.json({
        error: ERROR_AUTHENTICATION_FAILED,
        status: ERROR_CLIENT_CODE,
      });
    }

    const token = _encrypt(user.id);

    res.json({ status: "OK", user, token });
  } catch (error) {
    res.json({
      error: error,
      status: ERROR_SERVER_CODE,
    });
  }
};

const _encrypt = (id) => jwt.sign({ id }, SECRET_WORD, { expiresIn: TTL });

module.exports = {
  login,
};
