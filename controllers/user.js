const { create, findByPk, destroy, find_and_update } = require("../querys/user");
const bcrypt = require("bcrypt");
const { ERROR_SERVER, ERROR_SERVER_CODE, SUCCESS_USER_DELETE_MESSAGE } = require("../const/const");
const { find_user_role } = require("../querys/auth");
const { find_user_by_email } = require("../querys/auth");
const db = require("../models/index");

exports.getUsers = async (req, res) => {
  try {
    const data = await db.User.findAll();
    if (data) {
      res.send({
        users: data,
      });
    }
  } catch (error) {
    res.status(500).send({
      message: ERROR_SERVER,
    });
  }
};
// User registration with auto login and redirection to main page
exports.userRegister = async (req, res, next) => {
  try {
    const user = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: req.body.password /* bcrypt.hashSync(req.body.password, 10) */,
      image: req.body.image,
      organizationId: req.body.organizationId,
      roleId: req.body.roleId,
      createdAt: new Date(),
      updatedAt: new Date(),
    };
    const data = await db.User.create(user);
    // if the user is created correctly perform the login with next
    console.log("la DATA CREADA ES: ", data.dataValues);
    if (data.dataValues) {
      res.status(200).json({
        status: 200,
        data: data.dataValues,
        /* token: _encrypt(data.dataValues.id), */
      });
    } else {
      res.status(500).json({
        message: ERROR_SERVER,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: ERROR_SERVER,
    });
  }
};

// Update user
exports.userUpdate = async (req, res) => {
  const id = req.params.id;
  console.log("UPDATE BODY ", req.body);
  try {
    const data = await find_and_update(id, req.body);
    console.log("Obtained data ", data);
    
    if (!data) {
      res
        .status(ERROR_CLIENT_CODE)
        .json({ message: ERROR_DATA_NOT_FOUND_MESSAGE, data: data });
    } else {
      res.json({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: data,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
};

exports.userDelete = async (req, res) => {
  try {
    let user = await db.User.findByPk(req.params.id);

    if (!user) {
      return res.status(404).json({ msg: ERROR_USER_DELETE_MESSAGE });
    }

    await destroy(req.params.id);

    res.json({ message: SUCCESS_USER_DELETE_MESSAGE });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: ERROR_SERVER,
    });
  }
};

exports.findUsers = async (req, res) => {
  try {
    const { id } = req.params;
    const role = await find_user_role(id);
    res.json({ status: "ok", userRole: role });
  } catch (error) {
    throw error;
  }
};
