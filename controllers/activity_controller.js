const { 
  createActivity, 
  getActivity, 
  updateActivity,
  getActivities,
  find_and_delete 
} = require("../querys/activity");
const {
  SUCCESS_CODE,
  SUCCESS_MESSAGE,
  ERROR_DATA_NOT_FOUND_MESSAGE,
  ERROR_CLIENT_CODE,
  ERROR_SERVER_CODE,
  ERROR_SERVER,
  ERROR_ACTIVITY_DELETE_MESSAGE,
  SUCCESS_ACTIVITY_DELETE_MESSAGE
} = require("../const/const");
const { SUCCESS_UPDATE_ACTIVITY } = require('../const');

async function activity_post(req, res) {
  const { body: activity } = req;
  try {
    const data = await createActivity(activity);
    //console.log("se cargo esto :  ", data);

    if (data !== null) {
      res.json({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: data,
      });
    } else {
      res.json({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE,
        data: data,
      });
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).json({ message: ERROR_SERVER, data: error });
  }
}

const activity_get = async(req, res) => {
  try {
    const activity = await getActivity(req.params.id);

    if(activity){
      res.status(SUCCESS_CODE).send({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: activity,
      })
    }else{
      res.status(ERROR_CLIENT_CODE).send({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE, 
        data: activity       
      })
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).send({
      message: ERROR_SERVER,
      data: error
    })
  }
}

const activities_get = async(req, res) => {
  try {
    const activities = await getActivities();

    if(activities){
      res.status(SUCCESS_CODE).send({
        status: SUCCESS_CODE,
        message: SUCCESS_MESSAGE,
        data: activities,
      })
    }else{
      res.status(ERROR_CLIENT_CODE).send({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE, 
        data: activities       
      })
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).send({
      message: ERROR_SERVER,
      data: error
    })
  }
}

const activity_update = async(req, res) => {    
  try {
    const activity = await updateActivity(req.params.id, req.body);

    if(activity){
      res.status(SUCCESS_CODE).send({
        status: SUCCESS_CODE,
        message: SUCCESS_UPDATE_ACTIVITY
      })
    }else{
      res.status(ERROR_CLIENT_CODE).send({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE
      })
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).send({
      message: ERROR_SERVER,
      data: error
    })
  }
}

const activity_delete = async (req, res) => {
  try { 
    let activities = await getActivity(req.params.id);
      
    if (activities == null) {
        return res.status(404).json({ msg: ERROR_ACTIVITY_DELETE_MESSAGE })
    }

  await find_and_delete(req.params.id);

  res.json({ message: SUCCESS_ACTIVITY_DELETE_MESSAGE });
} catch (error) {
  console.log(error);
  res.status(500).send({
    message: ERROR_SERVER,
  });
}
}

module.exports = {
  activity_post,
  activity_get,
  activity_update,
  activities_get,
  activity_delete
};
