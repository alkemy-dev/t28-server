const yup = require("yup");
const {
  ERROR_CLIENT_CODE,
  ERROR_SERVER_CODE,
  ERROR_EMPTY_DATA_MESSAGE,
  ERROR_DATA_NOT_FOUND_MESSAGE,
  ERROR_SERVER
} = require("../const/const");

const { getActivity } = require('../querys/activity');

const activityValidationPost = async (req, res, next) => {
  console.log("Validating");
  try {
    const schema = yup.object().shape({
      name: yup.string().required(ERROR_EMPTY_DATA_MESSAGE),
      content: yup.string().required(ERROR_EMPTY_DATA_MESSAGE),
    });
    schema.validateSync(req.body);
    next();
  } catch (error) {
    res.json({
      status: ERROR_CLIENT_CODE,
      message: error.message,
      data: error,
    });
  }
};

const activityValidationUpdate = async(req, res, next) => {  
  try {
    const activity = await getActivity(req.params.id);

    if(activity){
      return next();
    }else{
      res.status(ERROR_CLIENT_CODE).send({
        status: ERROR_CLIENT_CODE,
        message: ERROR_DATA_NOT_FOUND_MESSAGE
      })    
    }
  } catch (error) {
    res.status(ERROR_SERVER_CODE).send({
      status: ERROR_SERVER_CODE,
      message: ERROR_SERVER
    })    
  }
}

module.exports = { 
  activityValidationPost, 
  activityValidationUpdate 
};
