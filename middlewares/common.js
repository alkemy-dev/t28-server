const { validationResult } = require('express-validator');
const AppError = require('../errors/appError');
const { ERROR_CLIENT_CODE, ERROR_VALIDATION, ERROR_DATA_NOT_FOUND_MESSAGE } = require('../const/const');

const validResult = (req, res, next) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        res.json({
            error: ERROR_VALIDATION,
            status: ERROR_CLIENT_CODE,
            data: errors.errors
        });
    }
    next();
}

module.exports = {
    validationResult: validResult
}