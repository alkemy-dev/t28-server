const { body, validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const { errorMinCharacters } = require('../const/index');
const { ERROR_NOT_EMPTY, ERROR_EMAIL, TOKEN_MESSAGE_FAIL, SECRET_WORD } = require('../const/const');
const AppError = require('../errors/appError');
const { findByPk } = require('../querys/user')

const userValidate = () => {
    return [        
        body('firstName')
            .notEmpty().withMessage(ERROR_NOT_EMPTY),
        body('lastName')
            .notEmpty().withMessage(ERROR_NOT_EMPTY),
        body('email')
            .notEmpty().withMessage(ERROR_NOT_EMPTY)
            .isEmail().withMessage(ERROR_EMAIL),        
        body('password')
            .notEmpty().withMessage(ERROR_NOT_EMPTY)
            .isLength({ min: 6 }).withMessage(errorMinCharacters(6))
    ]
}

const validate = (req, res, next) => {
    const errors = validationResult(req);

    if(errors.isEmpty()){
        return next();
    }

    const extractedErrors = [];
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }));

    return res.status(422).json({
        errors: extractedErrors,
    });
}

const verifyJWT = async (token) => {
    try {
        const {id} = jwt.verify(token, SECRET_WORD);
        const user = await findByPk(id);
        return user;
    } catch (error) {
        throw new AppError(TOKEN_MESSAGE_FAIL);
    }
           
};


module.exports = {
    userValidate,
    validate,
    verifyJWT
}