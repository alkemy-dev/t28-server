const { check } = require('express-validator');
const { validationResult } = require('../common');
const { find_user_by_email, find_user_role } = require("../../querys/auth");
const AppError = require('../../errors/appError');
const { ERROR_CLIENT_CODE, ERROR_USER_WITHOUT_PERMISSIONS, AUTHORIZATION, TOKEN_MESSAGE_SUCCESS, ADMIN_ROLE } = require('../../const/const');
const { verifyJWT } = require('../userValidate');

const _emailRequired = check('email', 'Email is required').not().isEmpty();

const _emailValid = check('email', 'Email is not valid').isEmail();

const _emailNotExists = check('email').custom(
    async (email = '') => {
        const userFound = await find_user_by_email(email);
        if (!userFound) {
            throw new AppError('Email does not exists in database', ERROR_CLIENT_CODE);
        }
    }
);

const _passwordRequired = check('password', 'Password is required').not().isEmpty();

const _checkUserAdmin = async (req, res, next) => {
    try {
        const userRole = await find_user_role(req.user.id);
        if (userRole !== ADMIN_ROLE) {
            throw new AppError(ERROR_USER_WITHOUT_PERMISSIONS, ERROR_CLIENT_CODE);
        }
    } catch (error) {
        throw error;
    }
    next();
}

const _idRequired = check('id').not().isEmpty();


const validJWT = async (req, res, next) => {

    const token = req.headers[AUTHORIZATION];
    if (!token) {
        throw new AppError(TOKEN_MESSAGE_SUCCESS);
    }

    try {
        const user = await verifyJWT(token);
        req.user = user;
        next();
    } catch (error) {
        throw error;
    }

};

const RequestValidation = [
    validJWT,
    _checkUserAdmin,
    _idRequired,
    validationResult,
];

const postLoginValidation = [
    _emailRequired,
    _emailValid,
    _emailNotExists,
    _passwordRequired,
    validationResult,
];

module.exports = {
    postLoginValidation,
    RequestValidation,
    validJWT,
}