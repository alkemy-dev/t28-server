const yup = require("yup");
const {
  ERROR_CLIENT_CODE,
  ERROR_EMPTY_DATA_MESSAGE,
  ERROR_NOT_TYPE_STRING_MESSAGE,
} = require("../const/const");

const category_validation_post = async (req, res, next) => {
  if (typeof req.body.name !== "string") {
    res.status(400).json({
      status: ERROR_CLIENT_CODE,
      message: ERROR_NOT_TYPE_STRING_MESSAGE,
    });
  } else {
    try {
      const schema = yup.object().shape({
        name: yup.string().required(ERROR_EMPTY_DATA_MESSAGE),
      });
      schema.validateSync(req.body);
      next();
    } catch (error) {
      res.status(400).json({
        status: ERROR_CLIENT_CODE,
        message: error.message,
        data: error,
      });
    }
  }
};

module.exports = { category_validation_post };
