const yup = require("yup");
const {
  ERROR_CLIENT_CODE,
  ERROR_EMPTY_DATA_MESSAGE,
} = require("../const/const");

const testimony_validation_post = async (req, res, next) => {
  try {
    const schema = yup.object().shape({
      name: yup.string().required(ERROR_EMPTY_DATA_MESSAGE),
      content: yup.string().required(ERROR_EMPTY_DATA_MESSAGE),
    });
    schema.validateSync(req.body);
    next();
  } catch (error) {
    res.status(400).json({
      status: ERROR_CLIENT_CODE,
      message: error.message,
      data: error,
    });
  }
};

module.exports = { testimony_validation_post };
