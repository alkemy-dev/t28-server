# Server Base - Proyecto ONG


## Envinroment setup

1) Create database 
``` mysql
CREATE DATABASE ong
```

2) Copy .env.example to .env and fill with database credentials.

To install dependencies, run
``` bash
npm install
```

3) Migrations:
``` bash
npx sequelize-cli db:migrate
```

4) Seeders:
``` bash
npx sequelize-cli db:seed:all
```

## Start local server

``` bash
npm start
```

## Run test
``` bash
npm test
```



## Users Example
```
    {
        firstName: `Usuario 0`,
        lastName: `User 0`,
        email: `a@a0.com`,
        password: "1234567",
        organizationId: 1,
        roleId: roleName,
        image:
          "https://media.istockphoto.com/photos/portrait-of-happy-indian-man-smiling-at-home-picture-id1270067432",
        createdAt: new Date(),
        updatedAt: new Date(),
      }
```